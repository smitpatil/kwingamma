#pragma once

struct GammaVals {
    // current values
    double red;
    double green;
    double blue;

    // initial values
    double i_red;
    double i_green;
    double i_blue;
};
