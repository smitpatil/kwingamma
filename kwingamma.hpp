#pragma once

#include <QDBusArgument>
#include <QStringListModel>

#include <KQuickAddons/ConfigModule>

#include "kwin_gamma.h"

#include "devicemodel.hpp"

class KWinGamma : public KQuickAddons::ConfigModule {
    Q_OBJECT

    Q_PROPERTY(bool syncAll MEMBER syncAll NOTIFY syncAllChanged)

public:
    KWinGamma(QObject* parent, const QVariantList& args);
    ~KWinGamma();

    void save() override;
    void defaults() override;
    void load() override;

    Q_INVOKABLE void setGamma(double r, double g, double b);

    Q_INVOKABLE QString testImgPath(QString img) const;

    void calNeedToSave();
    void calRepresentsDefaults();

Q_SIGNALS:

    void updateSliders(double red, double green, double blue);
    void syncAllChanged();

private:

    bool syncAll;

    void emitUpdateSliders();

    DeviceModel deviceModel;

    OrgKdeKWinGammaInterface dbusInterface;
};
