#include "devicemodel.hpp"

DeviceModel::DeviceModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int DeviceModel::rowCount([[maybe_unused]] const QModelIndex &parent) const
{
    return deviceNames.size();
}

QVariant DeviceModel::data(const QModelIndex &index, int role) const
{
    if (role == 1) {
        return deviceNames[index.row()];
    }
    return QVariant();
}

QHash<int, QByteArray> DeviceModel::roleNames() const
{
    return { { 1, "display" } };
}

void DeviceModel::removeDevice(const QString &device)
{
    auto index = deviceNames.indexOf(device);
    beginRemoveRows(QModelIndex(), index, index);

    if(index == activeIndex)
        Q_EMIT activeIndexUpdated(--activeIndex);

    deviceNames.removeAt(index);
    deviceGammaVals.remove(index);

    endRemoveRows();
}

void DeviceModel::addDevice(const QString &device, const GammaVals &gvals)
{
    auto index = deviceNames.count();
    beginInsertRows(QModelIndex(), index, index);

    deviceNames.append(device);
    deviceGammaVals.append(gvals);

    endInsertRows();
}

const QString DeviceModel::activeDeviceName()
{
    return deviceNames[activeIndex];
}

void DeviceModel::setGamma(double r, double g, double b)
{
    auto& dev = deviceGammaVals[activeIndex];
    dev.red = r;
    dev.green = g;
    dev.blue = b;

    Q_EMIT gammaChanged();
}

QVector<double> DeviceModel::getGamma()
{
    const auto &gamma = deviceGammaVals[activeIndex];
    return { gamma.red, gamma.green, gamma.blue };
}
