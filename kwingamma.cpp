
#include "kwingamma.hpp"

#include <KAboutData>
#include <KLocalizedString>
#include <KPluginFactory>

#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusReply>

#include <QDBusPendingCallWatcher>

#include <QDate>

#include <QDebug>

K_PLUGIN_CLASS_WITH_JSON(KWinGamma, "metadata.json")

static KAboutData* kwinGammaAboutData()
{
    KAboutData* data = new KAboutData;
    data->setLicense(KAboutLicense::GPL_V2);

    data->setComponentName(QStringLiteral("KWinGamma"));
    data->setDisplayName(QStringLiteral("KWinGamma"));
    data->setVersion("0.1");
    data->setCopyrightStatement(
        QStringLiteral("Copyright (C) 2022-%1 Smit S. Patil").arg(QDate::currentDate().year()));

    data->addAuthor(
        i18nc("@info:credit", "Smit S. Patil"),
        QStringLiteral("Developer and Maintainer"),
        QStringLiteral("smit17av@gmail.com"),
        QStringLiteral("https://smit17.netlify.app/"));

    return data;
}

KWinGamma::KWinGamma(QObject* parent, const QVariantList& args)
    : KQuickAddons::ConfigModule(parent, args)
    , deviceModel(this)
    , dbusInterface(
          QStringLiteral("org.kde.KWin"),
          QStringLiteral("/Gamma"),
          QDBusConnection::sessionBus(),
          this)
{

    setAboutData(kwinGammaAboutData());
    setButtons(Apply | Default);

    qmlRegisterSingletonInstance("org.kde.KWinGamma", 1, 0, "DeviceModel", &deviceModel);

    // get device list and load intial gamma values from kwin
    auto msg = dbusInterface.devices();
    connect(new QDBusPendingCallWatcher(msg), &QDBusPendingCallWatcher::finished, this, [this, msg](auto self) {
        const auto devices = msg.value();

        if (msg.isError()) {
            qDebug() << "ERROR getting device list:" << msg.error();
            return;
        }

        for (const auto& dev : devices) {
            dbusInterface.setActiveDevice(dev);

            auto gammaMsg = dbusInterface.get();
            gammaMsg.waitForFinished();

            const auto vec = gammaMsg.value();
            // clang-format off
            GammaVals gamma {
                vec[0], vec[1], vec[2],
                vec[0], vec[1], vec[2],
            };
            // clang-format on

            deviceModel.addDevice(dev, gamma);
        }

        dbusInterface.setActiveDevice(devices[0]);
        deviceModel.activeIndex = 0;

        calNeedToSave();
        calRepresentsDefaults();

        delete self;
    });

    auto handleActiveIndexChange = [this] {
        qDebug() << "active index changed";
        dbusInterface.setActiveDevice(deviceModel.activeDeviceName());
        emitUpdateSliders();
    };

    connect(&deviceModel, &DeviceModel::activeIndexChanged, this, handleActiveIndexChange);

    connect(&deviceModel, &DeviceModel::activeIndexUpdated, this, handleActiveIndexChange);

    connect(&dbusInterface, &OrgKdeKWinGammaInterface::deviceAdded, this, [this](const QString& device) {
        dbusInterface.setActiveDevice(device);
        auto gammaReply = dbusInterface.get();
        gammaReply.waitForFinished();

        dbusInterface.setActiveDevice(deviceModel.activeDeviceName());

        auto gamma = gammaReply.value();

        // clang-format off
        GammaVals gammavals = {
            gamma[0], gamma[1], gamma[2],
            gamma[0], gamma[1], gamma[2],
        };
        // clang-format on

        deviceModel.addDevice(device, gammavals);
    });

    connect(&dbusInterface, &OrgKdeKWinGammaInterface::deviceRemoved, this, [this](const QString& device) {
        deviceModel.removeDevice(device);
    });

    connect(&deviceModel, &DeviceModel::activeIndexChanged, this, [&] {
        dbusInterface.setActiveDevice(deviceModel.activeDeviceName());
    });
}

KWinGamma::~KWinGamma()
{
    // user did not saved so restore initial before closing
    if (needsSave()) {
        const auto& devNames = deviceModel.deviceNames;
        const auto& devGVals = deviceModel.deviceGammaVals;

        for (int i = 0; i < devNames.count(); i++) {
            const auto& dev = devGVals[i];
            dbusInterface.setActiveDevice(devNames[i]);
            dbusInterface.set(dev.i_red, dev.i_green, dev.i_blue);
        }
    }
}

void KWinGamma::setGamma(double red, double green, double blue)
{
    if (syncAll) {
        const auto& devNames = deviceModel.deviceNames;
        auto& devGVals = deviceModel.deviceGammaVals;

        for (int i = 0; i < devNames.count(); i++) {
            auto& dev = devGVals[i];
            dev.red = red;
            dev.green = green;
            dev.blue = blue;

            dbusInterface.setActiveDevice(devNames[i]);
            dbusInterface.set(dev.red, dev.green, dev.blue);
        }
        dbusInterface.setActiveDevice(deviceModel.activeDeviceName());
    } else {
        dbusInterface.set(red, green, blue).waitForFinished();
        deviceModel.setGamma(red, green, blue);
    }

    calNeedToSave();
    calRepresentsDefaults();
}

void KWinGamma::emitUpdateSliders()
{
    const auto gamma = deviceModel.getGamma();
    Q_EMIT updateSliders(gamma[0], gamma[1], gamma[2]);
}

// on reset button
void KWinGamma::load()
{
    const auto& devNames = deviceModel.deviceNames;
    auto& devGVals = deviceModel.deviceGammaVals;

    for (int i = 0; i < devNames.count(); i++) {
        auto& dev = devGVals[i];

        dev.red = dev.i_red;
        dev.green = dev.i_green;
        dev.blue = dev.i_blue;

        dbusInterface.setActiveDevice(devNames[i]);
        dbusInterface.set(dev.red, dev.green, dev.blue);
    }

    dbusInterface.setActiveDevice(deviceModel.activeDeviceName());

    emitUpdateSliders();

    calNeedToSave();
    calRepresentsDefaults();
}

// on save button
void KWinGamma::save()
{
    const auto& devNames = deviceModel.deviceNames;
    auto& devGVals = deviceModel.deviceGammaVals;

    for (int i = 0; i < devNames.count(); i++) {
        auto& dev = devGVals[i];

        dev.i_red = dev.red;
        dev.i_green = dev.green;
        dev.i_blue = dev.blue;

        dbusInterface.setActiveDevice(devNames[i]);
        dbusInterface.set(dev.red, dev.green, dev.blue);
    }

    dbusInterface.save();

    emitUpdateSliders();

    calRepresentsDefaults();
}

// on default button
void KWinGamma::defaults()
{
    const auto& devNames = deviceModel.deviceNames;
    auto& devGVals = deviceModel.deviceGammaVals;

    for (int i = 0; i < devNames.count(); i++) {
        auto& dev = devGVals[i];

        dev.red = 1;
        dev.green = 1;
        dev.blue = 1;

        dbusInterface.setActiveDevice(devNames[i]);
        dbusInterface.set(dev.red, dev.green, dev.blue);
    }

    dbusInterface.setActiveDevice(deviceModel.activeDeviceName());

    emitUpdateSliders();

    setRepresentsDefaults(true);

    calNeedToSave();
}

void KWinGamma::calNeedToSave()
{
    for (const auto& i : qAsConst(deviceModel.deviceGammaVals)) {
        if (!(i.red == i.i_red && i.green == i.i_green && i.blue == i.i_blue)) {
            setNeedsSave(true);
            return;
        }
    }
    setNeedsSave(false);
}

void KWinGamma::calRepresentsDefaults()
{
    for (const auto& i : qAsConst(deviceModel.deviceGammaVals)) {
        if (!(i.red == 1 && i.green == 1 && i.blue == 1)) {
            setRepresentsDefaults(false);
            return;
        }
    }
    setRepresentsDefaults(true);
}

QString KWinGamma::testImgPath(QString img) const
{
    return QStandardPaths::locate(
        QStandardPaths::StandardLocation::GenericDataLocation,
        QStringLiteral("kwingamma/pics/%1").arg(img));
}

#include "kwingamma.moc"
