#pragma once

#include <QAbstractListModel>

#include "gammavals.h"

class KWinGamma;

class DeviceModel : public QAbstractListModel {
    Q_OBJECT

    Q_PROPERTY(int activeIndex MEMBER activeIndex NOTIFY activeIndexChanged)

    friend KWinGamma;

private:
    QVector<QString> deviceNames;
    QVector<GammaVals> deviceGammaVals;

public:
    int activeIndex = -1;

    DeviceModel(QObject* parent);

    int rowCount(const QModelIndex& parent) const override;

    QVariant data(const QModelIndex& index, int role) const override;

    QHash<int, QByteArray> roleNames() const override;


    void removeDevice(const QString& device);

    void addDevice(const QString& device, const GammaVals& gvals);

    const QString activeDeviceName();


    void setGamma(double r, double g, double b);

    QVector<double> getGamma();

Q_SIGNALS:
    //emit from qml side
    void activeIndexChanged(int index);

    //emit from cpp side
    void activeIndexUpdated(int index);
    void gammaChanged();
};
