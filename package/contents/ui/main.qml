import QtQuick 2.12
import QtQuick.Controls 2.12

import org.kde.kirigami 2.7 as Kirigami
import org.kde.kcm 1.2
import org.kde.KWinGamma 1.0
import QtQuick.Layouts 1.15

SimpleKCM {

    id: root

    property var pics : [
        "cmyscale.png",
        "rgbscale.png",
        "darkgrey.png",
        "midgrey.png",
        "lightgrey.png"
    ]

    component ChannelSlider : RowLayout {
        property alias value : slider.value
        property alias color : slider.palette.highlight
        signal moved
        Kirigami.FormData.buddyFor: slider
        Slider {
            id : slider
            from: 0.1
            to:  3
            value : 1
            Layout.fillWidth: true
            onMoved: {
                parent.moved()
                root.setGamma()
            }
        }
        TextField {
            text : slider.value.toFixed(4);
            Layout.maximumWidth: 50
            validator : RegExpValidator { regExp : /[0-9]+\.[0-9]+/ }
            onTextEdited : slider.value = parseFloat(text)
        }
        // Item {}
    }

    component IndividualChannelSlider : ChannelSlider {
        onMoved: checkAllSliders()
    }

    component FormHLine : Kirigami.Separator {
        Kirigami.FormData.isSection: true
    }

    ColumnLayout {

        Kirigami.FormLayout {
            ComboBox {
                model : pics
                Kirigami.FormData.label : "Test Image"
                id : picsComboBox
                currentIndex : imgView.currentIndex
            }
        }

        SwipeView {
            id : imgView
            Layout.fillWidth: true
            Layout.preferredHeight : 200
            Layout.leftMargin : 30
            Layout.rightMargin : 30
            clip: true
            currentIndex : picsComboBox.currentIndex
            Repeater {
                model : pics
                Image {
                    property string img
                    source : kcm.testImgPath(modelData)
                    fillMode: Image.PreserveAspectFit
                }
            }
        }

        Kirigami.FormLayout {
            Layout.fillWidth: true

            FormHLine{}

            ChannelSlider {
                id : all
                Kirigami.FormData.label: "All:"
                opacity : red.value == green.value && red.value == blue.value ? 1 : 0.3
                color : "white"
                onMoved: {
                    red.value = value
                    green.value = value
                    blue.value = value
                }
            }

            FormHLine{}

            ChannelSlider {
                id : red
                Kirigami.FormData.label: "Red:"
                color : "red"
            }

            ChannelSlider {
                id : green
                Kirigami.FormData.label: "Green:"
                color : "lime"
            }

            ChannelSlider {
                id : blue 
                Kirigami.FormData.label: "Blue:"
                color : "blue"
            }

            FormHLine{}

            RowLayout {
                Kirigami.FormData.label: "Devices"
                ComboBox {
                    id : devices
                    model : DeviceModel
                    textRole : "display"
                    onModelChanged:  currentIndex = 0
                    onActivated:  DeviceModel.activeIndex = currentIndex

                    enabled: !syncAll.checked
                }

                 CheckBox {
                     id: syncAll
                     text: "Apply for all"
                     onToggled: kcm.syncAll = checked
                 }
            }
        }
    }

    Connections {
        target: kcm
        function onUpdateSliders(r,g,b) {
            red.value = r
            green.value = g
            blue.value = b
            checkAllSliders()
        }
    }

    Connections {
        target: DeviceModel
        function onActiveIndexUpdated(i) {
            devices.currentIndex = i
        }
    }


    function setGamma() {
        kcm.setGamma(red.value, green.value, blue.value)
    }

    function checkAllSliders() {
        if (red.value === blue.value && green.value === red.value){
            all.value = red.value
        }
    }
}
